<?php
/**
 * Created by PhpStorm.
 * User: tauke
 * Date: 27.12.2017
 * Time: 0:08
 */

namespace App\Http\Requests;


class QuoteFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "weight" => [
                "required",
                "regex:/(((-[1-9])([0-9]+)?)|(([1-9]+)?[0-9]))\.[0-9]+/"
            ],
            "height" => [
                "required",
                "regex:/(((-[1-9])([0-9]+)?)|(([1-9]+)?[0-9]))\.[0-9]+/"
            ],
            "length" => [
                "required",
                "regex:/(((-[1-9])([0-9]+)?)|(([1-9]+)?[0-9]))\.[0-9]+/"
            ],
            "width" => [
                "required",
                "regex:/(((-[1-9])([0-9]+)?)|(([1-9]+)?[0-9]))\.[0-9]+/"
            ],
            "from" => "required|array",
            "to" => "required|array",
            "address" => "required",
            "name" => "required",
            "volume" => "required",
            "price" => "required",
            "email" => "required|email",
            "quantity" => "integer|nullable|min:0",
            "insurance"=>"nullable|in:0,1",
            "telephone"=>"required"
        ];
    }
}