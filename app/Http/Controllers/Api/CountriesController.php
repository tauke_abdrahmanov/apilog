<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    public function index($query)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.exline.systems/public/v1/regions/destination?title='.$query);
        return $res->getBody();
    }
}
