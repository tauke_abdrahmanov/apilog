<?php

namespace App\Http\Controllers\Api;

use App\Classes\SmsSender;
use App\Http\Requests\QuoteFormRequest;
use App\Jobs\SendSmsJob;
use App\Mail\DeliveryRequest;
use App\Models\Quote;
use App\Models\Website;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function store(QuoteFormRequest $request)
    {
        $website = Website::where('url', $request->header('origin'))->first();
        if (!$website) {
            $website = Website::create([
                'url' => $request->header('origin'),
            ]);
        }
        $quote = Quote::create([
            'quantity' => $request->get('quantity'),
            'type' => $request->get('type'),
            'name' => $request->get('name'),
            'weight' => $request->get('weight'),
            'width' => $request->get('width'),
            'length' => $request->get('length'),
            'height' => $request->get('height'),
            'from_city' => $request->input('from.city.name'),
            'from_address' => $request->input('from.address'),
            'from_description' => $request->input('from.description'),
            'to_city' => $request->input('to.city.name'),
            'to_address' => $request->input('to.address'),
            'to_description' => $request->input('to.description'),
            'price' => $request->get('price'),
            'temperature' => $request->get('temperature'),
            'incoterms' => $request->get('incoterms'),
            'insurance' => $request->get('insurance'),
            'website_id' => $website->id,
            'user_agent' => $request->header('user-agent'),
            'quote_type' => 'delivery',
            'email' => $request->get('email'),
            'telephone' => $request->get('telephone'),
            'code' => $request->get('code'),
            'volume' => $request->get('volume'),

        ]);
        if(\App::environment("production")){
            Mail::queue(new DeliveryRequest($quote, $request->get('company'), $request->get('delivery_type')));
            if($website->telephone != "" || $website->telephone != null){
                SendSmsJob::dispatch();
            }
        }

    }
}
