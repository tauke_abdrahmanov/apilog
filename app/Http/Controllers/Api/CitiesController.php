<?php

namespace App\Http\Controllers\Api;

use App\Classes\SmsSender;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CitiesController extends Controller
{
    public function index($query)
    {
        $key = env('GOOGLE_API_KEY', 'AIzaSyAoplOgPG3ujbO5OHuQhTxA9yUyjA-2GI8');
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='.$query.'&types=(regions)&key='.$key);
        $response = json_decode($res->getBody(), true);
        $arr = [];
        foreach ($response['predictions'] as $key =>$value) {
             $arr[] = [
                'id' =>  $value['id'],
                'name' => $value['description']
             ];
        }
        return $arr;
    }
}
