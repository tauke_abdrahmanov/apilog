<?php

namespace App\Jobs;

use App\Classes\SmsSender;
use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $recepient;
    private $quote;
    private $from;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recepeint,Quote $quote,$from = "")
    {
        $this->quote = $quote;
        $this->recepient = $recepeint;
        if($from != null || $from != ""){
            $this->from = $from;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        SmsSender::sendSms($this->recepient,$this->quote,$this->from);
    }
}
