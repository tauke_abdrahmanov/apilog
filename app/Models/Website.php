<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $fillable = [
        'url',
        'user_id',
    ];

    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }
}
