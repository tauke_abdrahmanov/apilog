<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'quantity',
        'type',
        'name',
        'weight',
        'width',
        'length',
        'height',
        'from_description',
        'from_city',
        'from_address',
        'to_description',
        'to_city',
        'to_address',
        'price',
        'temperature',
        'incoterms',
        'insurance',
        'website_id',
        'user_agent',
        'quote_type',
        'email',
        'telephone',
        'code',
        'volume'
    ];

    public function website()
    {
        $this->belongsTo(Website::class);
    }
}
