<?php
/**
 * Created by PhpStorm.
 * User: tauke
 * Date: 07.01.2018
 * Time: 21:13
 */

namespace App\Classes;

use App\Models\Quote;
use App\Models\Website;
use GuzzleHttp\Client;

class SmsSender
{
    // Отправка смс письма
    public static function sendSms($to ,Quote $quote,$from = ""){
        // Токен для авторизации в сервисе smsapi.com
        $token = config("smsapi.token");
        // обрезаем полный адрес до 128 символов
        $fromAddress = str_limit($quote->from_city." ".$quote->from_address,128);
        $toAddress = str_limit($quote->to_city." ".$quote->to_address,128);
        // Тело смс письма
        $message = "Откуда:{$fromAddress}\n".
            "Куда:{$toAddress}\n".
            "Email:{$quote->email}\n".
            "Telephone:{$quote->telephone}";
        $body = ["to"=>$to,"message"=>$message,"format"=>"json"];
        // Имя отправителя смс
        if($from != "" || $from != null){
            $body["from"] = $from;
        }
        // JWT авторизация используя токен
        $headers =
            [
                'Authorization' => "Bearer {$token}"
            ];
         $client = new Client(["base_uri"=>"https://api.smsapi.com"
         ]);
         $res = $client->post("sms.do",["headers"=>$headers, "form_params"=>$body]);
         $stringBody = (string)$res->getBody();
         $jsonResult = json_decode($stringBody,true);
         // Если произошла ошибка в сервисе отправок смс, то логируем его
         if(isset($jsonResult["error"])){
             \Log::error("sms.error - ".$stringBody);
         }
    }
}