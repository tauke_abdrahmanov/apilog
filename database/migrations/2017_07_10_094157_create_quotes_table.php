<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity');
            $table->text('type')->nullable();
            $table->text('name')->nullable();
            $table->float('weight');
            $table->float('width');
            $table->float('length');
            $table->float('height');
            $table->text('from_description')->nullable();
            $table->string('from_city');
            $table->string('from_address')->nullable();
            $table->text('to_description')->nullable();
            $table->string('to_city');
            $table->string('to_address')->nullable();
            $table->string('price')->nullable();
            $table->string('temperature')->nullable();
            $table->string('incoterms')->nullable();
            $table->boolean('insurance')->nullable();
            $table->text('user_agent');
            $table->string('quote_type');
            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('volume')->nullable();
            $table->string('code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
