<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/cities/{query}', 'Api\CitiesController@index');
Route::get('/countries/{query}', 'Api\CountriesController@index');
Route::post('/quotes', 'Api\QuotesController@store');
Route::post('/email', 'Api\EmailController@store');
