Кол-во: {{ $quote->quantity }} <br>
Тип груза: {{ $quote->type }} <br>
Тип доставки {{ $delivery_type }}
Название: {{ $quote->name }} <br>
Вес: {{ $quote->weight }} <br>
Ширина: {{ $quote->width }} <br>
Длина: {{ $quote->length }} <br>
Высота: {{ $quote->height }} <br>
Откуда описание: {{ $quote->from_description }} <br>
Откуда: {{ $quote->from }} <br>
Куда описание: {{ $quote->to_description }} <br>
Куда: {{ $quote->to }} <br>
Стоимость: {{ $quote->price }} <br>
Температура: {{ $quote->temperature }} <br>
Incoterms: {{ $quote->incoterms }} <br>
Страховка: @if($quote->insurance)
                Да <br>
           @else
                Нет <br>
           @endif
Емайл: {{ $quote->email }} <br>
Тел: {{ $quote->telephone }} <br>
Код груза: {{ $quote->code }} <br>
Объем: {{ $quote->volume }} <br>